#include <stdio.h>
/**
 * @input A : Read only ( DON'T MODIFY ) Integer array
 * @input n1 : Integer array's ( A ) length
 * @input B : Integer
 * 
 * @Output Integer
 */
int findLow(const int* A, int n1, int B, int flag){
    int low=0,high=n1-1,result=0,mid;
    while(low<=high){
        mid=low+(high-low)/2;
        if(A[mid]==B){
            result = mid;
            if(flag==1){
                high = mid-1; //checks first
            }
            else{
                low = mid+1; //checks last
            }
        }
        else if (B< A[mid]){
            high = mid-1;
        }
        else {
            low = mid + 1;
        }
    }
    return result;
}
int findCount(const int* A, int n1, int B) {
    int lowest,highest;
    if(n1==1){
        if(A[0]==B)return 1;
        else return 0;
    }
    lowest = findLow(A,n1,B,1);
    else{
       highest = findLow(A,n1,B,0);
    }
    return highest-lowest+1;
}
int main() {
    /*
    int myarray [133] ={1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,2,2,2,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,4,4,4,4,4,4,4,4,4,4,4,5,5,5,5,5,5,5,5,5,5,5,5,6,6,6,6,6,6,6,6,6,6,6,6,6,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,8,8,8,8,8,8,8,8,9,9,9,9,9,9,9,9,9,9,9,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10};
	int B = 11;
	int number=0;
	number=findCount(myarray,(sizeof(myarray)/sizeof(int)),B);
	printf("They are %d number of %d",number,B);
	*/
	int myarray [34] = {1,1,2,2,2,2,3,3,3,3,3,3,3,4,4,5,5,5,5,5,6,6,6,7,7,8,8,8,8,9,9,10,10,10};
    int B=1;
    int number=0;
    number=findCount(myarray,(sizeof(myarray)/sizeof(int)),B);
	printf("They are %d number of %d",number,B);
	
	return 0;
}