/**
 * Instructions
 * For a certain quantity, identify the number of bills that are needed of the following denominations:    $200, $50, $20 y $1
 * Example:
 * If the quantity is: $1343
 * You will need:
 * 6 bills of $200
 * 2 bills of $50
 * 2 bills of $20
 * 3 bills of $1
 * 
 * @input amount : Integer
 * @Output print amout of bills and denomination
 */
//Includes and defines section
#include <stdio.h>
//Main
int main(){
    int amount,bills=200,amountbills;
    printf("Which is the amount you want to identify: $");
    scanf("%d",&amount);
    printf("Se necesitan: \n");
    amountbills=amount/bills;
    if(amountbills>0){
        printf("%d billetes de $200\n",amountbills);
    }
    amount%=bills;
    bills=50;
    amountbills=amount/bills;
    if(amountbills>0){
        printf("%d billetes de $50\n",amountbills);
    }
    amount%=bills;
    bills=20;
    
    amountbills=amount/bills;
    if(amountbills>0){
        printf("%d billetes de $20\n",amountbills);
    }
    amount%=bills;
    bills=1;
    amountbills=amount/bills;
    if(amountbills>0){
        printf("%d billetes de $1\n",amountbills);
    }
	return 0;
}
