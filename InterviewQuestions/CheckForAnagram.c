/**
 * This is problem 3 for Wizeline interview the programming language is C
 * Autor of the code is Tlakaelel Axayakatl Ceja Valadez
 * Instructions
 * Write a function that receives an Array of words and prints a clasification of these words by its anagram.
 * Input:
 * var words = ['AMOR', 'XISELA', 'JAMON', 'ROMA', 'OMAR', 'MORA', 'ESPONJA', 'RAMO', 'JAPONES', 'ARMO', 'MOJAN', 'MARO', 'ORAM', 'MONJA', 'ALEXIS'];
 *
 * Output:
 * AMOR - ROMA - OMAR - MORA - RAMO - ARMO - MARO - ORAM
 * MONJA - JAMON - MOJAN
 * ESPONJA - JAPONES
 * ALEXIS - XISELA
 */
//Includes and defines section
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define NUMBER_OF_STRINGS 15

//Prototipes
void CheckForAnagram (char* words[], char numberOfStrings);
void PrintWord (char* words[], int index,char flag);
char SortAndCompare(char* string1, char* string2,int lenght);
void quickSort(char A[], int si, int ei);
int partition(char A[], int si, int ei);
void exchange(char *a, char *b);

//Main
int main (){
    char* words[NUMBER_OF_STRINGS]={"AMOR", "XISELA", "JAMON", "ROMA", "OMAR", "MORA", "ESPONJA", "RAMO", "JAPONES", "ARMO", "MOJAN", "MARO", "ORAM", "MONJA", "ALEXIS"};
    CheckForAnagram(words,NUMBER_OF_STRINGS);
    return 0;
}

/**
 * This functions create a char array to keeptrack of which strings already passed and are anagrams, 0 for a word that haven't
 * haven't passed through indexword and is not an anagram and 1 for a word that already passed through indexword or was an anagram
 * indexword -> is the frist word available according to the keeptrack array
 * indexpivot -> are the different indexes words through the array
 */
void CheckForAnagram (char* words[], char numberOfStrings){
    int indexword = 0, indexpivot = 1, index;
    int keeptrack[numberOfStrings];

    //First check if the words array is empty
    if (words==NULL){
        return ;
    }
    //Init the keeptrack array
    keeptrack[0]=1;
    for(index = 1; index < numberOfStrings; index++){
        keeptrack[index]=0;
    }
    //Print first word which is the one to begin
    PrintWord(words,indexword,1);

    //Real magic begins here
    while(indexword < numberOfStrings){
        //First check the lenght, no need to sort if not equal
        if(strlen(words[indexword])!=strlen(words[indexpivot])){
            indexpivot++;
        }
        else{
            printf("%d",strlen(words[indexword]));
            if(SortAndCompare(words[indexword],words[indexpivot],strlen(words[indexword]))){
                //This case is if they are not anagrams
                indexpivot++;
            }
            else{
                //In this case are anagrams, so we print the word
                keeptrack[indexpivot]=1;
                PrintWord(words,indexpivot,0);
                indexpivot++;
            }
        }
        if(indexpivot==numberOfStrings){
            printf("\n");
            while(keeptrack[indexword]!=0 && indexword<numberOfStrings){
                indexword++;
            }
            if(indexword<numberOfStrings){
                keeptrack[indexword]=1;
                PrintWord(words,indexword,1);
                indexpivot=indexword+1;
                while(keeptrack[indexpivot]!=0 && indexpivot<numberOfStrings){
                    indexpivot++;
                }
            }
        }
    }
}
/**
 * This function is only to print a word.
 * In order to maintain the proposed Output
 * Output:
 * AMOR - ROMA - OMAR - MORA - RAMO - ARMO - MARO - ORAM
 * MONJA - JAMON - MOJAN
 * ESPONJA - JAPONES
 * ALEXIS - XISELA
 *
 * I used a flag
 * If it's the first word just print the word
 * If it's an anagram put " - " before printing the word
 */
void PrintWord (char* words[], int index, char flag){
    if(flag){
        printf("%s",words[index]);
    }
    else{
        printf(" - %s",words[index]);
    }
}

/**
 * This is an implementation of QuickSort Algorithm for two strings
 * After doing the sort it will compare and:
 * Return 0 if they are anagrams
 * Return 1 if they are not anagrams
 */
char SortAndCompare(char* string1, char* string2,int lenght){
    quickSort(string1,0,lenght-1);
    quickSort(string2,0,lenght-1);

    if(strcmp(string1,string2)==0){
        return 0;
    }
    else{
        return 1;
    }
}
void exchange(char *a, char *b)
{
    printf("%s",b);
    char temp;
    temp = *a;
    *a   = *b;
    *b   = temp;
}
int partition(char A[], int si, int ei)
{
    char x = A[ei];
    int i = (si - 1);
    int j;

    for (j = si; j <= ei - 1; j++)
    {
        if(A[j] <= x)
        {
            i++;
            exchange(&A[j], &A[x]);
        }
    }
    exchange (&A[i + 1], &A[ei]);
    return (i + 1);
}
void quickSort(char A[], int si, int ei)
{
    int pi;    /* Partitioning index */
    if(si < ei)
    {
        pi = partition(A, si, ei);
        quickSort(A, si, pi - 1);
        quickSort(A, pi + 1, ei);
    }
}