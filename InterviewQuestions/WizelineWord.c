/**
 * This is problem 1 for Wizeline interview the programming language is C
 * Autor of the code is Tlakaelel Axayakatl Ceja Valadez
 * Instructions
 * Write a program that prints the numbers from 1 to 100. with the following conditions:
 *      For multiples of three [3] print “Wize” instead of the numbers
 *      For the multiples of five [5] print “line” instead of the number.
 *      For numbers which are multiples of both three [3] and five[5] print “Wizeline” instead of the number.
 */
//Includes and defines section
#include <stdio.h>
#define multiplecondition1 3
#define multiplecondition2 5
//Main
void main(){
    char num;
    for(num=1;num<101;num++){           //One less ASM instruction by not doing the equal comparison
        //Check First condition
        if (num % multiplecondition1 == 0){
            printf("Wize");
        }
        //Check Second condition
        if (num % multiplecondition2 == 0){
            printf("line");
        }
        //Print only the number
        if(num % multiplecondition1 !=0 && num % multiplecondition2 !=0){
            printf("%d",num);
        }
        printf(" ");
    }
    //End of Line
    printf("\n");
}
