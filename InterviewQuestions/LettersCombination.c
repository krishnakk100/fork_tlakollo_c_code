/**
 * This is a problem for Oracle interview, the programming language is whatever the applicant wants
 * Instructions
 * Write a program that receives a text file containing a number in each row and after that a list of alphabet letters, eg:
 * 0={f,j,c,a}
 * 1={h,i}
 * 2={m,z,b}
 * 3={l,q,w,e}
 * 4={y}
 * 5={n,p}
 * 
 * The program also receives a String that contains 2 or more numbers, eg: '0435'
 * The program should output the different posible combinations that can be made with those numbers in the specific order, eg:
 * If using 0435 as the string:
 * fyln
 * fylp
 * fyqn
 * fyqp
 * fywn
 * fywp
 * fyen
 * fyep
 * jyln
 * jylp
 * jyqn
 * jyqp
 * ...
 * 
 */
 
//Includes and defines section
#include <stdio.h>
#include <stdlib.h>

//Structures
typedef struct Node {
    char data;
    struct Node* next;
}Node;

//Prototypes
void insert (int value);
char findCircularList(Node* head);

//Global variables
Node * head;

//Main
void main (void){
    char value;
    head = NULL;
    char result[3];
    
    //Test Case 1: Use a NULL linked list
    result [0]= findCircularList(head);
    
    //Fill the linked list
    for(value='E'; value>='A';value--){
        insert(value);
    }
    //Test Case 2: Check what happens with a null terminated linked list
    result [1]= findCircularList(head);
    
    //Join Last node 'E' to third node 'C'
    head->next->next->next->next->next = head->next->next;
    //Test Case 3: Check for a cycled linked list
    result [2]= findCircularList(head);
}
//Functions implementation
/* This function inserts a node at the beginning of the List*/
void insert (int value){
    Node* temp = (Node*) malloc(sizeof(Node*));
    temp->data = value;
    temp->next = head;
    head = temp;
}
/* This function checks for a cyclic linked list
 * Returns 0 if in the final node is pointing to NULL 
 * Returns -1 if the value inserted as head was a null value
 * Returns 1 when the linked list is cyclic, doesn't return which node was the one that initiate the cycle
 * 
 * It will also print info about the result in console
 */
char findCircularList(Node* head){
    Node *temp, *helper;
    if(head==NULL){
        printf("Error! D:\nThe value inserted as head was Null\n\n");
        return 0;
    }
    temp = head;
    helper = head->next;
    //While the faster pointer doesn't reach any NULL node
    while (helper!=NULL && helper->next!=NULL){
        if(helper==temp || helper->next == temp){           //Check if a collision happened
            printf("Finish!\nThis is a cyclic Linked List\n\n");
            return 1;
        }
        else{                                               //If they didn't collide advance the pointers
            temp = temp->next;
            helper = helper->next->next;
        }
    }
    printf("Finish!\nThe linked list was not cyclic\n\n");
    return 0;
}